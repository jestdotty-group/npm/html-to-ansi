const assert = require('assert')
const htmlToANSI = require('../index.js')

it('new line', ()=> assert.equal(htmlToANSI('abc<br>abc'), 'abc\nabc'))
it('color', ()=> assert.match(htmlToANSI('abc <font color="#5cb85c">abc</font>'), /abc .*abc.*/))
it('etc', ()=> assert.equal(htmlToANSI('abc <befwerr>abc<open> something</open>'), 'abc abc something'))
